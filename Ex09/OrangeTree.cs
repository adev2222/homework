namespace Ex09;

public class OrangeTree
{
    public int age { get; set; } = 0;

    public int height { get; set; } = 0;
    public bool treeAlive { get; set; } = true;
    public int numOranges { get; private set; } = 0;
    public int OrangesEaten { get; private set; } = 0;

    public void OneYearPasses()
    {
        OrangesEaten = 0;
        age += 1;
        
        if(age >= 80)
        {
            treeAlive = false;
        }
        else
        {
            height += 2;
        }
        
        
        if (age is < 2 or > 80)
        {
            numOranges = 0;
        }
        else
        {
            numOranges = age * 5 - 5;
        }
    }
    

    public void EatOrange(int count)
    {
        if (numOranges > count)
        {
            OrangesEaten += count;
            numOranges -= count;
        }
    }
    
    
}