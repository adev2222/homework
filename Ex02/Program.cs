﻿using System.Diagnostics.Metrics;

namespace Ex02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string name;

            int age;

            Console.Write("Skriv dit navn: ");
            name = Console.ReadLine();

            Console.Write("Skriv dit alder: ");
            age = Int32.Parse(Console.ReadLine());


            Console.WriteLine(value: $"{name} er {age} år gammel og er {AlderBeskrivelse(age)}");


            
        }

        public static string AlderBeskrivelse(int age)
        {
            if(age > 0 &&  age < 13) {
                return "et barn";
            
            }else if(age > 12 && age < 20)
            {
                return "en teenager";
            }else if(age >19 && age < 26)
            {
                return "en studerende";
            }else if(age > 25 && age < 67) {

                return "i arbejde";
            }
            else
            {
                return "en pensionist";
            }
        }
    }


    
}