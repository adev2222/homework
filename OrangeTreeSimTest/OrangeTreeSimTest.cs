using Ex09;

namespace OrangeTreeSimTest;

[TestClass]
public class UnitTest1
{

   
    OrangeTree _orangeTree = new OrangeTree();
    [TestInitialize]
    public void SetupForTest()
    {
        _orangeTree.age = 0;
        _orangeTree.height = 6;
        _orangeTree.treeAlive = true;
        
    }
    
    [TestMethod]
    public void ShouldIncrementTheTreesAgeWithEachPassingYear()
    {
        //Act
        _orangeTree.OneYearPasses();

        //Assert
        Assert.AreEqual(1, _orangeTree.age);
    }
            [TestMethod]
        public void ShouldIncrementTheTreesHeightByTwoWithEachPassingYear()
        {
            //Act
            _orangeTree.OneYearPasses();

            //Assert
            Assert.AreEqual(8, _orangeTree.height);
        }
        [TestMethod]
        public void ShouldDieAfter80Years()
        {
            //Act
            for (int i = 1; i <= 80; i++)
            {
                _orangeTree.OneYearPasses();
            }

            //Assert
            Assert.AreEqual(false, _orangeTree.treeAlive);
        }

        [TestMethod]
        public void ShouldProduceFruitAfter2Years()
        {
            _orangeTree.OneYearPasses();
            Assert.AreEqual(0, _orangeTree.numOranges);

            _orangeTree.OneYearPasses();
            Assert.AreEqual(5, _orangeTree.numOranges);

        }
        [TestMethod]
        public void ShouldIncreaseFruitProductionBy5PiecesEachYearAfterMaturity()
        {
            _orangeTree.OneYearPasses();
            _orangeTree.OneYearPasses();
            Assert.AreEqual(5, _orangeTree.numOranges);

            _orangeTree.OneYearPasses();
            Assert.AreEqual(10, _orangeTree.numOranges);

        }
        [TestMethod]
        public void ShouldCountNumberOfOrangesEatenThisYear()
        {
            _orangeTree.OneYearPasses();
            _orangeTree.OneYearPasses();
            _orangeTree.EatOrange(1);

            Assert.AreEqual(1, _orangeTree.OrangesEaten);

            _orangeTree.EatOrange(3);
            Assert.AreEqual(4, _orangeTree.OrangesEaten);

            Assert.AreEqual(1, _orangeTree.numOranges);
        }
        [TestMethod]
        public void OrangesEatenOneYearPasses()
        {
            //Act
            _orangeTree.OneYearPasses();
            _orangeTree.OneYearPasses();
            _orangeTree.EatOrange(1);
            _orangeTree.EatOrange(3);
            _orangeTree.OneYearPasses();
            //Assert
            Assert.AreEqual(0, _orangeTree.OrangesEaten);
            Assert.AreEqual(10, _orangeTree.numOranges);
        }

        [TestMethod]
        public void DeadTreeDoNotGrowAndProduceFruit()
        {
            //Act
            for (int i = 1; i <= 80; i++)
            {
                _orangeTree.OneYearPasses();
            }

            //Assert
            Assert.AreEqual(false, _orangeTree.treeAlive);
          
            _orangeTree.OneYearPasses();
            Assert.AreEqual(0, _orangeTree.numOranges);
            Assert.AreEqual(164, _orangeTree.height);
            Assert.AreEqual(81, _orangeTree.age);
            _orangeTree.OneYearPasses();
            Assert.AreEqual(0, _orangeTree.numOranges);
            Assert.AreEqual(164, _orangeTree.height);
            Assert.AreEqual(82, _orangeTree.age);
    

        }



}