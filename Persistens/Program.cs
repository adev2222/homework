﻿


namespace Persistens;


internal class Program
{
    static void Main(string[] args)
    {
        // Person person = new Person("Anders Andersen", new DateTime(1975, 8, 24), 175.9, true, 3);
        //
        // //Console.WriteLine(person.MakeTitle());
        //
        // DataHandler handler = new DataHandler("Data2.txt");
        // handler.SavePerson(person);
        //
        // Person person2 = handler.LoadPerson();
        //
        // Console.WriteLine(person2.MakeTitle());
        DataHandler handler2 = new DataHandler("Data5.txt");
        Person[] persons = new Person[]
        {
            new Person("William Jensen", new DateTime(1975, 8, 24), 175.9, false, 2),
            new Person("Alfred Nielsen", new DateTime(1991, 3, 12), 185.0, true, 3),
            new Person("Oskar Hansen", new DateTime(2005, 11, 9), 183.2, true, 1),
            new Person("Emma Pedersen", new DateTime(2013, 9, 25), 113.7, false, 0),
            new Person("Alma Andersen", new DateTime(1982, 1, 5), 169.9, false, 2),
            new Person("Clara Christensen", new DateTime(1999, 7, 13), 165.3, true, 0),
        };

        // #### ACT ####
        handler2.SavePersons(persons);

        Console.WriteLine(handler2.LoadPersons()[3].MakeTitle());
    }
}