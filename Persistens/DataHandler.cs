namespace Persistens;

public class DataHandler
{
    public string DataFileName { get;}

    public DataHandler(string dataFileName)
    {
        DataFileName = dataFileName;
    }

    public void SavePerson(Person person)
    {
        using StreamWriter writer = new StreamWriter(DataFileName, false);
        writer.WriteLine(person.MakeTitle());
    }

    public  Person LoadPerson()
    {
        try
        {
            using StreamReader reader = new StreamReader(DataFileName);
            string text = reader.ReadLine();

            string[] array = text.Split(";");

            Person person = new Person(array[0], DateTime.Parse(array[1]), Convert.ToDouble(array[2]),
                Convert.ToBoolean(array[3]), Convert.ToInt32(array[4]));
            return person;
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
 
    }

    public void SavePersons(Person[] persons)
    {
        try
        {
            using StreamWriter writer = new StreamWriter(DataFileName, true);
            foreach (Person person in persons)
            {
                writer.WriteLine(person.MakeTitle()); 
            }

        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
   

    }

    public Person[] LoadPersons()
    {
        List<Person>? list = new List<Person>();

        try
        {
            using StreamReader reader = new StreamReader(DataFileName);
            string? line;
            while ((line = reader.ReadLine()) != null)
            {
                Console.WriteLine(line);
                string[] array = line.Split(";");
                list.Add(new Person(array[0], DateTime.Parse(array[1]), Convert.ToDouble(array[2]),
                    Convert.ToBoolean(array[3]), Convert.ToInt32(array[4])));
            }
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw;
        }
        return list.ToArray();
    }
}