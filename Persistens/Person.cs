using System.Diagnostics;

namespace Persistens;

public class Person
{
    private string _name;
    public string Name
    {
        get { return _name; }
        set
        {
            if (String.IsNullOrEmpty(value))
            {
                ErrorMessage();
            }

            _name = value;
        }
    }

    private DateTime _dateTime;
    public DateTime BirthDate
    {
        get => _dateTime;
        set
        {
            if (Convert.ToInt32(value.Year) <= 1900)
            {
                ErrorMessage();
            }

            _dateTime = value;
        }
    }

    private double _height;
    public double Height
    {
        get => _height;
        set
        {
            if (value <= 0)
            {
                ErrorMessage();
            }

            _height = value;
        }
    }

    public bool IsMarried { get; set; }

    private int _noOfChildren;
    public int NoOfChildren
    {
        get => _noOfChildren;
        set
        {
            if (value < 0)
            {
                ErrorMessage();
            }

            _noOfChildren = value;
        }
    }


    public Person(string name, DateTime birthDate, double height, bool isMarried, int noOfChildren)
    {
        Name = name;
        BirthDate = birthDate;
        Height = height;
        IsMarried = isMarried;
        NoOfChildren = noOfChildren;
    }

    // chaining
    public Person(string name, DateTime birthDate, double height, bool isMarried) :
        this(name, birthDate, height, isMarried, 0)
    {
    }

    public void ErrorMessage() =>
        throw new Exception("Der er opstået en fejl … eller hvad der nu er relevant at skrive her");


    public string MakeTitle()
    {
        return $"{Name};{BirthDate.ToString("dd-MM-yyyy HH':'mm':'ss")};{Height};{IsMarried};{NoOfChildren}";
    }
}