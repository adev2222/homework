﻿namespace Ex01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            string name;

            int age;

            Console.Write("Skriv dit navn: ");
            name = Console.ReadLine();

            Console.Write("Skriv dit alder: ");
            age = Int32.Parse(Console.ReadLine());


            Console.WriteLine($"{name} er {age} år gammel.");

        }
    }
}