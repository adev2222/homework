﻿namespace Ex03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, World!");
        }
    }

    class Person
    {
        public string name { get; set; }
        public int age { get; set; }
    }
}