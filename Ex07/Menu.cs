namespace Ex07;

public class Menu
{
    public string Title;
    private MenuItem[] MenuItems = new MenuItem[10];
    private int ItemCount = 0;


  
    public Menu(string title)
    {
        Title = title;
    }

    public void AddMenuItem(string menuTitle)
    {
        MenuItem mi = new MenuItem(menuTitle);
        MenuItems[ItemCount] = mi;
        ItemCount++;
    }
    
    public void Show()
    {
        Console.WriteLine(Title);
        for (int i = 0; i < ItemCount; i++)
        {
            Console.WriteLine(MenuItems[i].Title);
        }
    }

    public void SelectMenuItem()
    {

        int num = EnTalFunc(ItemCount);
        Console.Clear();
        if (num == 0) 
        {
                Environment.Exit(0);
        }
        else
        {   
                num -= 1;
                Console.WriteLine();
                Console.BackgroundColor = ConsoleColor.Yellow;
                Console.WriteLine($" Du har valgt: {MenuItems[num].Title}");
                Console.ResetColor();
                Console.WriteLine();
        }
    }
    
    private static int EnTalFunc(int menuLength)
    {
        int num;
        while (true) 
        {
            if (int.TryParse(Console.ReadLine(), out num) && num < menuLength)
            {
                break; // Exit the loop if input is a valid integer
            }
            else
            {
                Console.BackgroundColor = ConsoleColor.Red;
                Console.ForegroundColor = ConsoleColor.White;
                Console.Write($"Du skal skrive et tal mellem 1 og {menuLength}. Prøv igen: ");
                Console.ResetColor();
            }
        }
        return num;
    }
}