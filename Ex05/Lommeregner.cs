﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ex05
{
    internal class Lommeregner
    {

        public int Add(int x, int y)
        {
            return x + y;
        }

        public int Substract(int x,int y) { return x - y; }

        public double Divide(int x, int y) { return x / y; }


        public int Multiply(int x, int y ) { return x * y; }
    }
}
