﻿using System;
using System.Security.Cryptography.X509Certificates;

namespace Ex05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello, Lommeregner!");


            Lommeregner lommeregner= new Lommeregner();

            do
            {
                Console.Write("Skriv første tal: ");
                int one = EnTalFunc();

                Console.Write("Skriv anden tal: ");
                int two = EnTalFunc();

                Console.Write("Skriv + - / *: ");
                string plusminus = Console.ReadLine();

                double result;

                while (true) { 
                if (plusminus == "+")
                {
                    result = lommeregner.Add(one, two);
                        break;
                }
                else if (plusminus == "-")
                {
                    result = lommeregner.Substract(one, two);
                        break;
                    }
                else if (plusminus == "/")
                {

                    result = lommeregner.Divide(one, two);
                        break;
                    }
                else if(plusminus == "*")
                {
                    result = lommeregner.Multiply(two, one);
                        break;
                    }

                    Console.Write("Skriv + - / *: ");
                    plusminus = Console.ReadLine();


                }

                Console.WriteLine($"{one} {plusminus} {two} = {result}");

                Console.WriteLine("Skriv slut hvis du er færdig, ellers klik på en knap");
                if (Console.ReadLine() == "slut")
                {
                    break;
                }


            } while (true);



            int EnTalFunc()
            {
                int num;
                while (true) 
                { 
           
                    if (int.TryParse(Console.ReadLine(), out num))
                        {
                            break; // Exit the loop if input is a valid integer
                        }
                    else
                    {
                    Console.Write("Det er ikke et tal. Skriv igen: ");
                    }
                }
                return num;
            }

 

        }
    }
}